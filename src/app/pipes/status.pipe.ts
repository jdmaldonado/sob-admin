import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const status = {
      'ASSIGNED': 'Contactado(a)',
      'EXPIRED': 'Vencido(a)',
      'FREE': 'Usuario(a)',
      'NOT_INTERESTED': 'No está interesado(a)',
      'PREMIUM': 'Premium',
      'UNASSIGNED': 'Sin Llamar',
      default: 'Sin Llamar'
    }
    return status[value] || status['default'];
  }

}
