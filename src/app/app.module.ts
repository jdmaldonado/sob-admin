import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Routing } from './app.routing';
import { environment } from '../environments/environment';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// tslint:disable-next-line:max-line-length
import { MatCheckboxModule, MatSidenavModule, MatCardModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatIconModule, MatToolbarModule, MatListModule, MatDialogModule, MatOptionModule, MatSelectModule, MatProgressBarModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTabsModule, MatTooltipModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { MyProspectsComponent } from './components/my-prospects/my-prospects.component';
import { ProspectCardComponent } from './components/prospect-card/prospect-card.component';
import { ProspectFormComponent } from './components/prospect-form/prospect-form.component';
import { ProspectManagementComponent } from './components/prospect-management/prospect-management.component';
import { UnassignedProspectsComponent } from './components/unassigned-prospects/unassigned-prospects.component';

import { AuthGuard } from './guards/auth.guard';

import { StatusPipe } from './pipes/status.pipe';

import { AuthService } from './services/auth.service';
import { ManagementService } from './services/management.service';
import { OptionsService } from './services/options.service';
import { ProspectService } from './services/prospect.service';
import { UserService } from './services/user.service';
import { UtilsService } from './services/utils.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    MyProspectsComponent,
    ProspectCardComponent,
    ProspectFormComponent,
    ProspectManagementComponent,
    UnassignedProspectsComponent,
    StatusPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase, 'sob-admin'),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    FlexLayoutModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    Routing,
    // Material
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatOptionModule,
    MatProgressBarModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatSortModule,
    MatTabsModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
  ],
  entryComponents: [
    ProspectFormComponent,
    ProspectManagementComponent,
  ],
  providers: [
    AuthGuard,
    FormBuilder,
    // Services
    AuthService,
    ManagementService,
    OptionsService,
    ProspectService,
    UserService,
    UtilsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
