import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { MyProspectsComponent } from './components/my-prospects/my-prospects.component';
import { UnassignedProspectsComponent } from './components/unassigned-prospects/unassigned-prospects.component';

import { AuthGuard } from './guards/auth.guard';

const appRoutes: Routes = [
	{
		path: '',
		component: HomeComponent,
		canActivate: [AuthGuard,],
		children: [
			{
				path: '',
				component: UnassignedProspectsComponent
			},
			{
				path: 'my-prospects',
				component: MyProspectsComponent
			}
		]
	},
	{
		path: 'login',
		component: LoginComponent
	},
];

export const Routing = RouterModule.forRoot(appRoutes);