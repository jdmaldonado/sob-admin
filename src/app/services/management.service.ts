import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ManagementService {

  prospectManagements: AngularFireList<any[]>

  constructor(private _db: AngularFireDatabase) { }

  getManagementsByProspect(prospectId: string): Observable<any[]> {
    this.prospectManagements = this._db.list<any[]>(`managements/${prospectId}`);

    return this.prospectManagements
      .snapshotChanges()
      .map(managements => {
        return managements.map(management => ({ key: management.key, ...management.payload.val() }))
      })
      .map((items) => {
        return items.map(item => {
          item.user = this._db.object(`users/${item.userId}`).valueChanges()
          return item;
        })
      })
  }

  addManagement(data: any) {
    data.createdDate = new Date().getTime();
    return this.prospectManagements.push(data);
  }

}
