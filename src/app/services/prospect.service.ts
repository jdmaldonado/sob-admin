import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProspectService {

  _collectionName: string = 'prospects';

  constructor(private _db: AngularFireDatabase) { }

  addProspect(data: any) {
    return this._db.list<any>(`${this._collectionName}`).push(data);
  }

  getAllUnAssigned(): Observable<any> {
    return this._db.list<any>(`${this._collectionName}`, ref => ref.orderByChild('consultant').equalTo(null))
      .snapshotChanges()
      .map(prospects => {
        return prospects.map(prospect => ({ key: prospect.key, ...prospect.payload.val() }));
      });
  }

  getProspectById(prospectId: string): Observable<any> {
    return this._db.object<any>(`${this._collectionName}/${prospectId}`)
      .snapshotChanges()
      .map(prospect => ({ key: prospect.key, ...prospect.payload.val() }));
  }

  getProspectsByProperty(property: string, value: string): Observable<any[]> {
    return this._db.list<any>(`${this._collectionName}`, ref => ref.orderByChild(property).equalTo(value))
      .snapshotChanges()
      .map(prospects => {
        return prospects.map(prospect => ({ key: prospect.key, ...prospect.payload.val() }));
      });
  }

  updateProspect(prospectId: string, data: any) {
    return this._db.list<any>(`${this._collectionName}`).update(prospectId, data);
  }
}