import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {

  constructor(private _auth: AngularFireAuth) { }

  getAuthUser(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._auth.authState
        .subscribe(authUser => {
          resolve(authUser);
        },
        (err) => reject(err));
    })
  }

  getAuthUserObservable(): Observable<any> {
    return this._auth.authState;
  }

  createAuthUser(_email: string, _password: string): Promise<any> {
    return this._auth.auth.createUserWithEmailAndPassword(_email, _password);
  };

  loginWithPassword(account: any): Promise<any> {
    return this._auth.auth.signInWithEmailAndPassword(account.email, account.password);
  };

  resetPassword(email: string): Promise<any> {
    return firebase.auth().sendPasswordResetEmail(email);
  };

  logout(): void {
    this._auth.auth.signOut();
  };

}