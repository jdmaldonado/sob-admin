import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/add/operator/concatMap';

@Injectable()
export class OptionsService {

  constructor(
    private _db: AngularFireDatabase,
    private _http: Http
  ) { }

  getDepartments(): Observable<any[]> {
    return this._http.get('assets/jsons/colombia.json')
      .map((res: any) => res.json())
      .map((items) => {
        return items.map(item => ({ id: item.id, name: item.departamento }))
      })
  }

  getCitiesByDepartmentName(departmentName: string): Observable<any> {
    return this._http.get('assets/jsons/colombia.json')
      .map((res: any) => res.json())
      .concatMap((res) => res)
      .filter((department: any) => department.departamento === departmentName)
      .map(department => department.ciudades)
  }

  getStatus(): string[] {
    return [
      'NOT_INTERESTED'
    ]
  }

  getAsociations(): Observable<any[]> {
    return this._db.list<any>('associations').valueChanges();
  }

}