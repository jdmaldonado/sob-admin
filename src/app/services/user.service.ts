import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

  constructor(private _db: AngularFireDatabase) { }

  getUserById(userId: string): Observable<any> {
    return this._db.object<any>(`users/${userId}`)
      .snapshotChanges()
      .map(prospect => ({ key: prospect.key, ...prospect.payload.val() }));
  }

}
