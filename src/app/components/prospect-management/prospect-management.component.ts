import { Component, Input, ViewChild, OnInit, Inject } from '@angular/core';
import { MatSort, MatTableDataSource, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { AuthService } from '../../services/auth.service';
import { ManagementService } from '../../services/management.service';
import { UtilsService } from '../../services/utils.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-prospect-management',
  templateUrl: './prospect-management.component.html',
  styleUrls: ['./prospect-management.component.scss']
})
export class ProspectManagementComponent implements OnInit {


  @ViewChild(MatSort) sort: MatSort;

  private _loggedUserId: string;
  private _prospect: any;
  private _managementsSubscription: Subscription;
  public managementForm: FormGroup;
  public displayedColumns = ['description', 'createdDate', 'user'];
  public dataSource: MatTableDataSource<Management>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _authService: AuthService,
    private _dialogRef: MatDialogRef<ProspectManagementComponent>,
    private _formBuilder: FormBuilder,
    private _managementService: ManagementService,
    private _utilsService: UtilsService,
  ) {
    this._initForm();
  }

  ngOnInit() {
    this._setLoggedUser();
    if (this.data && this.data.prospect) {
      this._prospect = this.data.prospect;
      this._getManagements();
    }
  }

  ngOnDestroy() {
    if (this._managementsSubscription) {
      this._managementsSubscription.unsubscribe();
    }
  }

  private _getManagements(): void {
    this._managementsSubscription = this._managementService.getManagementsByProspect(this._prospect.key)
      .subscribe(
      (managementsData) => {
        this.dataSource = new MatTableDataSource(managementsData);
        this.dataSource.sort = this.sort;
      },
      (err) => console.log(err)
      )
  }

  private _initForm(): void {
    this.managementForm = this._formBuilder.group({
      description: ['', Validators.required]
    });
  }

  private _setLoggedUser(): void {
    this._authService.getAuthUser()
      .then((user) => this._loggedUserId = user.uid)
      .catch((err) => this._utilsService.showMessage(err.message));
  }

  addManagement(): void {
    this.managementForm.value.userId = this._loggedUserId;
    this._managementService.addManagement(this.managementForm.value)
      .then(() => {
        this._utilsService.showMessage('Seguimiento agregado con éxito')
        this.managementForm.reset();
      });
  }

  public dismiss(): void {
    this._dialogRef.close();
  }

}
export interface Management {
  id: string;
  description: string;
  createdDate: string;
  userId: string;
  user: Observable<any>
}
