import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { UtilsService } from '../../services/utils.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {

  public accountForm: FormGroup;

  constructor(
    private _authService: AuthService,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _utils: UtilsService
  ) {
    this._initForm();
  }

  private _initForm(): void {
    this.accountForm = this._formBuilder.group({
      email: ['', Validators.email],
      password: ['', Validators.required]
    });
  }

  doLogin() {
    this._authService.loginWithPassword(this.accountForm.value)
      .then(response => {
        this._router.navigate(['/']);
      })
      .catch(err => {
        this._utils.showMessage(err);
      });
  };


}
