import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ProspectFormComponent } from '../prospect-form/prospect-form.component';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  constructor(
    private _authService: AuthService,
    private _dialog: MatDialog,
  ) { }

  createProspect(): void {
    let dialogRef = this._dialog.open(ProspectFormComponent, {
      width: '500px',
      height: '100%',
    });
  }

  logOut(): void {
    this._authService.logout();
  }

}
