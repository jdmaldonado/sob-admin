import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AuthService } from '../../services/auth.service';
import { ProspectService } from '../../services/prospect.service';

@Component({
  selector: 'app-my-prospects',
  templateUrl: './my-prospects.component.html',
  styleUrls: ['./my-prospects.component.scss']
})
export class MyProspectsComponent implements OnInit {

  public assigned: any[];
  public users: any;
  public premium: any;
  public expired: any;
  public notInterested: any;

  constructor(
    private _authService: AuthService,
    private _prospectService: ProspectService,
  ) { }

  ngOnInit() {
    this._authService.getAuthUser()
      .then((user) => {
        if (user && user.uid) {
          this.setProspects(user.uid);
        }
        else {
          this.assigned = null;
          this.users = null;
          this.premium = null;
          this.expired = null;
        }
      })
  }

  setProspects(userId: string): void {
    // this._prospectService.getProspectsByProperty('consultant', userId)
    //   .subscribe((prospects) => {
    //     this.assigned = prospects.filter((prospect) => prospect.status === 'ASSIGNED');
    //     this.users = prospects.filter((prospect) => prospect.status === 'FREE');
    //     this.premium = prospects.filter((prospect) => prospect.status === 'PREMIUM');
    //     this.expired = prospects.filter((prospect) => prospect.status === 'EXPIRED');
    //     this.notInterested = prospects.filter((prospect) => prospect.status === 'NOT_INTERESTED');
    //   })
  }

}
