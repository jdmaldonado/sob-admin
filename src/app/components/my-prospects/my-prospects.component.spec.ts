import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyProspectsComponent } from './my-prospects.component';

describe('MyProspectsComponent', () => {
  let component: MyProspectsComponent;
  let fixture: ComponentFixture<MyProspectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyProspectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyProspectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
