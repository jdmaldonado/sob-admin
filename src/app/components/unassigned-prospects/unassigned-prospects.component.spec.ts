import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnassignedProspectsComponent } from './unassigned-prospects.component';

describe('UnassignedProspectsComponent', () => {
  let component: UnassignedProspectsComponent;
  let fixture: ComponentFixture<UnassignedProspectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnassignedProspectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnassignedProspectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
