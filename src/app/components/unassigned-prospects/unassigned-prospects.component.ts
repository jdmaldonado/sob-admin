import { Component, OnInit } from '@angular/core';
import { ProspectService } from '../../services/prospect.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-unassigned-prospects',
  templateUrl: './unassigned-prospects.component.html',
  styleUrls: ['./unassigned-prospects.component.scss']
})
export class UnassignedProspectsComponent implements OnInit {

  public unassigned: any[];
  public notInterested: any[];

  constructor(
    private _prospectService: ProspectService, ) { }

  ngOnInit() {
    // this._prospectService.getAllUnAssigned()
    //   .subscribe((prospects) => {
    //     this.unassigned = prospects.filter((prospect) => prospect.status === 'UNASSIGNED');
    //     this.notInterested = prospects.filter((prospect) => prospect.status === 'NOT_INTERESTED');
    //   });
  }

}
