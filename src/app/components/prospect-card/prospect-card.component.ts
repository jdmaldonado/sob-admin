import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ProspectFormComponent } from '../prospect-form/prospect-form.component';
import { ProspectManagementComponent } from '../prospect-management/prospect-management.component';

import { AuthService } from '../../services/auth.service';
import { ProspectService } from '../../services/prospect.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'prospect-card',
  templateUrl: './prospect-card.component.html',
  styleUrls: ['./prospect-card.component.scss']
})
export class ProspectCardComponent implements OnInit {

  @Input() prospect;

  public isOwner: Boolean;
  public buttonDissabled: Boolean = false;

  constructor(
    private _authService: AuthService,
    private _dialog: MatDialog,
    private _prospectService: ProspectService,
  ) { }

  ngOnInit() {
    this._authService.getAuthUser()
      .then((user) => {
        if (user && user.uid) {
          this._setIsOwner(user.uid);
        } else {
          this.isOwner = false;
        }
      });
  }

  private _setIsOwner(userId): void {
    if (this.prospect && this.prospect.consultant) {
      this.isOwner = (this.prospect.consultant === userId);
    } else {
      this.isOwner = false;
    }
  }

  openEditDialog(prospectId: string): void {
    this._dialog.open(ProspectFormComponent, {
      width: '500px',
      height: '100%',
      data: { prospectId }
    });
  }

  openManagementsDialog(): void {
    this._dialog.open(ProspectManagementComponent, {
      width: '80%',
      height: '100%',
      data: { prospect: this.prospect }
    });
  }

  openAppProfile(code: string): void {
    window.open(`http://app.socialbusiness.com.co/profile/${code}`, '_blank');
  }

  changePendingStatus(): void {
    this.buttonDissabled = true;
    const pending = !this.prospect.pending;

    this._prospectService.updateProspect(this.prospect.key, { pending })
      .then(() => { this.buttonDissabled = false; })
      .catch((err) => {
        console.log(err);
        this.buttonDissabled = false;
      });
  }

  openChat(prospect: any): void {
    window.open(`https://api.whatsapp.com/send?phone=57${prospect.cellPhone}`, '_blank');
  }

}
