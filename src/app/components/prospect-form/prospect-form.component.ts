import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import { UserService } from '../../services/user.service';

import { ProspectManagementComponent } from '../prospect-management/prospect-management.component';

import { OptionsService } from '../../services/options.service';
import { ProspectService } from '../../services/prospect.service';
import { UtilsService } from '../../services/utils.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { AuthService } from '../../services/auth.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'prospect-form',
  templateUrl: './prospect-form.component.html',
  styleUrls: ['./prospect-form.component.scss']
})
export class ProspectFormComponent implements OnInit {

  private _loggedUserId: string;
  private _prospectId: string;
  private _prospectSubscription: Subscription;

  public assignToMe: Boolean = false;
  public associations: Array<any>;
  public cities: Observable<any[]>;
  public departments: Observable<any[]>;
  public isMyProspect: Boolean;
  public isUnassigned: Boolean = true;
  public numberValidated: Boolean = false;
  public prospectCopy: any;
  public prospectForm: FormGroup;
  public showLoading: Boolean = false;
  public statusArray: string[];
  public ableToChangeStatus: Boolean = false;
  public userInfo: Observable<any>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _authService: AuthService,
    private _dialog: MatDialog,
    private _dialogRef: MatDialogRef<ProspectFormComponent>,
    private _formBuilder: FormBuilder,
    private _optionsService: OptionsService,
    private _prospectService: ProspectService,
    private _userService: UserService,
    private _utilsService: UtilsService,
  ) {
    this._initForm();
    this._initOptions();
  }

  ngOnInit() {
    this._setLoggedUser();
    if (this.data) {
      this._prospectId = this.data.prospectId;
      // console.log(this._prospectId);
      this.loadProspect();
    }
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    if (this._prospectSubscription) {
      this._prospectSubscription.unsubscribe();
    }
  }

  private _createNewProspect(): void {

    this._emailExist(this.prospectForm.value.email)
      .then((emailExist) => {
        if (emailExist) {
          return Promise.reject({ message: 'Este correo ya se encuentra usado por otro prospecto' });
        } else {
          this.prospectForm.controls['cellPhone'].patchValue(this.prospectForm.value.cellPhone.toString());
          this.prospectForm.controls['creationDate'].patchValue(new Date().getTime());
          return this._prospectService.addProspect(this.prospectForm.value);
        }
      })
      .then(() => {
        this.showLoading = false;
        this._utilsService.showMessage('Prospecto creado satisfactoriamente! :)');
        this.dismiss();
      })
      .catch((err) => {
        this.showLoading = false;
        this._utilsService.showMessage(err.message);
      });
  }

  private _emailExist(email: string): Promise<boolean> {

    return new Promise((resolve, reject) => {
      if (!email) {
        return resolve(false);
      }

      this._prospectService.getProspectsByProperty('email', email)
        .map(prospects => prospects.length > 0)
        .subscribe(emailExist => {
          resolve(emailExist);
        });
    });

  }

  private _initForm(): void {
    this.prospectForm = this._formBuilder.group({
      association: [null],
      cellPhone: [null, Validators.required],
      city: [null],
      code: [null],
      comment: [null],
      consultant: [null],
      creationDate: [null],
      modificationDate: [null],
      department: [null],
      email: [null],
      identification: [null],
      lastName: [null],
      name: [null],
      phone: [null],
      realState: [null],
      status: [null],
    });
  }

  private _initOptions(): void {
    this.departments = this._optionsService.getDepartments();
    this.statusArray = this._optionsService.getStatus();
    this._optionsService.getAsociations()
      .subscribe((associations) => this.associations = associations);
  }

  private _isValidForm(): any {

    if (this.prospectForm.value.email) {
      if (!this._utilsService.isEmail(this.prospectForm.value.email)) {
        return {
          error: 'Formato de correo inváido',
          status: false
        };
      }
    }

    return {
      status: true
    };
  }

  private _loadProspect(prospect: any): void {

    this._prospectId = prospect.key;
    this.prospectCopy = prospect;

    this.isMyProspect = (prospect.consultant === this._loggedUserId);
    this.isUnassigned = (prospect.consultant) ? false : true;

    if (!this.isMyProspect && !this.isUnassigned) {
      this._setUserInformation(prospect.consultant);
    }

    this.cities = this._optionsService.getCitiesByDepartmentName(prospect.department);
    this.statusArray.push(prospect.status);
    if (prospect.status === 'UNASSIGNED') {
      this.statusArray.push('ASSIGNED');
    }

    if (['UNASSIGNED', 'ASSIGNED', 'EXPIRED'].indexOf(prospect.status) > -1) {
      this.ableToChangeStatus = true;
    }

    this.prospectForm.patchValue(prospect);

    this._showLoadMessage();

  }

  private _setLoggedUser(): void {
    this._authService.getAuthUser()
      .then((user) => this._loggedUserId = user.uid)
      .catch((err) => this._utilsService.showMessage(err.message));
  }

  private _evaluateStatus(): void {
    if (this.prospectForm.value.consultant === null) {
      const newStatus = this.prospectForm.value.status || 'UNASSIGNED';
      this.prospectForm.controls['status'].patchValue(newStatus);
    } else {
      const newStatus = (this.prospectForm.value.status === 'UNASSIGNED') ? 'ASSIGNED' : (this.prospectForm.value.status || 'ASSIGNED');
      this.prospectForm.controls['status'].patchValue(newStatus);
    }
  }

  private _setUserInformation(userId: string): void {
    this.userInfo = this._userService.getUserById(userId);
  }

  private _showLoadMessage(): void {
    if (!this.isMyProspect && !this.isUnassigned) {
      this._utilsService.showMessage('Este número ya se encuentra ASIGNADO ');
    } else if (!this.isMyProspect && this.isUnassigned) {
      this._utilsService.showMessage('Este número ya está registrado, pero no se encuentra asignado');
    } else if (this.isMyProspect) {
      this._utilsService.showMessage('Este prospecto es tuyo(a)');
    }
  }

  private _updateProspect(): void {
    if (this.prospectCopy.email !== this.prospectForm.value.email) {

      this._emailExist(this.prospectForm.value.email)
        .then((emailExist) => {
          if (emailExist) {
            return Promise.reject({ message: 'Este correo ya se encuentra usado por otro prospecto' });
          } else {
            this.prospectForm.controls['cellPhone'].patchValue(this.prospectForm.value.cellPhone.toString());
            this.prospectForm.controls['modificationDate'].patchValue(new Date().getTime());
            this._prospectService.updateProspect(this._prospectId, this.prospectForm.value);
          }
        })
        .then(() => {
          this.showLoading = false;
          this._utilsService.showMessage('Prospecto actualizado satisfactoriamente! :)');
          this.dismiss();
        })
        .catch((err) => {
          this.showLoading = false;
          this._utilsService.showMessage(err.message);
        });
    } else {
      this.prospectForm.controls['cellPhone'].patchValue(this.prospectForm.value.cellPhone.toString());

      this._prospectService.updateProspect(this._prospectId, this.prospectForm.value)
        .then(() => {
          this.showLoading = false;
          this._utilsService.showMessage('Prospecto actualizado satisfactoriamente! :)');
          this.dismiss();
        })
        .catch((err) => {
          this.showLoading = false;
          this._utilsService.showMessage(err.message);
        });

    }
  }

  public statusSelectionChange(event): void {
    if (event.value === 'ASSIGNED') {
      this.assignToMe = true;
    } else if (event.value === 'UNASSIGNED') {
      this.assignToMe = false;
    }
  }

  public dismiss(): void {
    this._dialogRef.close();
  }

  public loadProspect(): void {
    this.showLoading = true;

    this._prospectSubscription = this._prospectService.getProspectById(this._prospectId)
      .subscribe(
        (prospect) => {
          this._loadProspect(prospect);
          this.showLoading = false;
          this.numberValidated = true;
        },
        (err) => console.log(err));
  }

  public onDepartmentChanged(event): void {
    const departmentName = event.value;
    this.cities = this._optionsService.getCitiesByDepartmentName(departmentName);
  }

  public openManagementsDialog(): void {
    this._dialog.open(ProspectManagementComponent, {
      width: '80%',
      height: '100%',
      data: { prospect: this.prospectCopy }
    });
  }

  public openAppProfile(code: string): void {
    window.open(`http://app.socialbusiness.com.co/profile/${code}`, '_blank');
  }

  public openChat(phoneNumber: string): void {
    window.open(`https://api.whatsapp.com/send?phone=57${phoneNumber}`, '_blank');
  }

  public saveProspect(): void {
    this.showLoading = true;

    const validation = this._isValidForm();

    if (validation.status && !validation.error) {
      if (this.assignToMe) {
        this.prospectForm.controls['consultant'].patchValue(this._loggedUserId);
      }

      this._evaluateStatus();

      if (this._prospectId) {
        this._updateProspect();
      } else {
        this._createNewProspect();
      }
    } else {
      this._utilsService.showMessage(validation.error);
      console.error(validation.error);
    }
  }

  public validateCellPhone(cellPhone: number): void {
    if (cellPhone.toString().length !== 10) {
      this._utilsService.showMessage('El número de celular debe tener 10 dígitos');
      return;
    } else {
      this.showLoading = true;
      this._prospectSubscription = this._prospectService.getProspectsByProperty('cellPhone', cellPhone.toString())
        .subscribe(
          (prospects) => {
            if (prospects && prospects.length > 0) {
              this._loadProspect(prospects[0]);
            } else {
              this.ableToChangeStatus = true;
              this._utilsService.showMessage('Número nuevo');
            }
            this.showLoading = false;
            this.numberValidated = true;
          },
          (err) => console.log(err));
    }
  }

}
