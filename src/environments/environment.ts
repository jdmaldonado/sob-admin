// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAILBasO7EykNhmFpb23P4rDr4TYTtrJvI',
    authDomain: 'sob-admin.firebaseapp.com',
    databaseURL: 'https://sob-admin.firebaseio.com',
    projectId: 'sob-admin',
    storageBucket: '',
    messagingSenderId: '934038425448'
  }
};
